﻿// Pract_16_5_Array.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <time.h>

using namespace std;

int DayOfMonth()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    return buf.tm_mday;
}


int main()
{
    const int ArrayDim = 5;

    int RowToSum = 0;
    int SumOfRow = 0;
    int Array[ArrayDim][ArrayDim] = { {} };

    RowToSum = DayOfMonth() % ArrayDim;

    for (int i = 0; i < ArrayDim; i++)
    {
        for (int j = 0; j < ArrayDim; j++)
        {   
            Array[i][j] = i + j;

            if (i == RowToSum)
            {
                SumOfRow += Array[i][j];
            }
            cout << Array[i][j] << ' ';
        }
        cout << '\n';
    }
    cout << "Sum of row " << RowToSum << " is " << SumOfRow;
}
