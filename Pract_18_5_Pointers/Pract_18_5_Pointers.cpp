﻿#include <iostream>
#include <string>
using namespace std;
class Player {
public: 
    int Score = 0;
    string PlayerName;

    Player(): PlayerName(""), Score(0) 
    {}
    Player(string _PlayerName, int _Score): PlayerName(_PlayerName), Score(_Score) 
    {}

    void Show() 
    {
        cout << PlayerName << " Score: " << Score << "\n";
    }
    
};
int n;
Player* players;
void bubblesort(Player* APlayer)
{
    for (int i = 0; i < n-1; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (APlayer[i].Score < APlayer[j].Score)
            {
                swap(APlayer[i], APlayer[j]);
            }
        }

     }

}
    
int main()
{  
    cout << "Count of players: ";
    cin >> n;
    Player* players{new Player[n]};
    for (int i = 0; i < n; i++)
    {      
        string Name = "";
        cout << "Name ";
        int Score;
        cin >> Name;
        cout << "Score ";
        cin >> Score;
        cout << "\n";
        Player player(Name, Score);
        players[i] = player;
    }

    cout << "Before sorting:\n";
    for (int i = 0; i < n; i++)
    {
        players[i].Show();
    }

    bubblesort(players);

    cout << "After sorting:\n";
    for (int i = 0; i < n; i++)
    {
       players[i].Show();
    }
    
    delete []players;
    players = nullptr;
}
