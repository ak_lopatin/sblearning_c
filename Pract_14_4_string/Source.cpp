#include <iostream>
void main()
{
	std::string MyString = "HelloWorld!";

	std::cout << MyString << '\n';
	std::cout << "Length:" << MyString.length() << '\n';
	std::cout <<"First char: " << MyString[0] << '\n';
	std::cout <<"Last char: " << MyString[MyString.length() - 1] << '\n';
}