﻿#include <iostream>
#include <string>
using namespace std;
class Animal
{
private:
    string Speech;
public:
    virtual string GetSpeech() 
    {
        return Speech;
    };
    virtual void SetSpeech(string OriginalVoice) 
    {
        Speech = OriginalVoice;
    };
    Animal() {};
    Animal(string ASpeech) :Speech(ASpeech) {}
   
    virtual void Voice() {};
    //{
    //    cout << GetSpeech() << "\n";
    //};
    
};

class Dog : public Animal
{
    
public:
    Dog() :Animal("Woof") {}
    void Voice() override
    {
        cout << "Dog bark: " << GetSpeech() << "\n";

    }
};

class Cat : public Animal
{
public:
    Cat() :Animal("Meau") {}

    void Voice() override
    {
        cout << "Cat say: " << GetSpeech() <<"\n";
    }
};

class Cow : public Animal
{
 
public:
    Cow():Animal("Moooo")
    {}
    void Voice() override
    {
        cout << "Cow say: " << GetSpeech() << "\n";
    }
};

int main()
{
    Animal* animals[15] = { new Animal[15] };
    
    for (int i = 0; i < 15; i++)
    {
        int r = rand() % 3;
        switch (r)
        {
        case 0:
        {
            animals[i] = new Dog();
            break;
        };
        case 1:
        {
            animals[i] = new Cow();
            break;
        };
        case 2:
        {
            animals[i] = new Cat();
            break;
        };
        }

        animals[i] ->Voice();
    }
    for (int i = 14; i >=0 ; i--)
    {
        delete animals[i];
        animals[i] = nullptr;
    }
    return 0;
}