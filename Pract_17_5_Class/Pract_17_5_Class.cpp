﻿// Pract_17_5_Class.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

class Vector
{   private:
        double x;
        double y;
        double z;
    public: 
        Vector() : x(0), y(0), z(0)
        {}
        Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
        {}
        void Show() const
        {
            cout << '\n' << x << ' ' << y << ' ' << z << '\n';
        }
        double Length()
        {
            return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        }
};

int main()
{
    Vector v(5,3,4);
    v.Show();
    cout << v.Length();
}