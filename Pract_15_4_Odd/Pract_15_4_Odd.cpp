﻿// Pract_15_4_Odd.cpp : Практическая работа №15.4
// Вывод четных / нечетных чисел не превышающих Max
// Odd - переменная отвечающая за четность результат
// Odd = false - четные
// Odd = True - нечетные

#include <iostream>
void FindOdd(int Max, bool Odd)
{
    for (int i = 0; 2 * i + Odd <= Max; i++)
    {
        std::cout << i * 2 + Odd;
        std::cout << "\n";
    }
}

int main()
{
    FindOdd(20, false);       
}
